# Postman (for your HTTP testing)
### Saving data from responces to enviorment
You can create a enviorment that you run your requests from, and add some scripts to the `tests` tab to perhaps save something from the responce body to a enviorment variable. This is useful for saving tokens.

In this case I have a enviorment variable called `jwt`
```
const response = pm.response.json();
pm.environment.set("jwt", response.tokenId);
```

the responce body
```
{
    "id": 1,
    "fullName": "Stefanía Reynis",
    "email": "stefania@ru.is",
    "tokenId": 5
}
```