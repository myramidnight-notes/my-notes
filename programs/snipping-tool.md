
# The sceenshot snipping tools (windows)
This is how you share screenshots, when you just want to show sections of the screen. It is very rare that we actually want to share the whole screen, just what we're trying to draw attention to.

There are pictures of the two tools, __Snipping tool__ and the new __Snip & Sketch__.

## Snipping tool (old)
This is the older version that they want to replace with _Snip & Sketch_. I prefer this older version, it's much faster.

![Snipping tool](./images/snipping-tool.png)

* Lets you write ontop of the snapshot
* Offers to open the snapshot in _3D-paint_, which has image cropping, rainbow-colored tear-drop like icon
* Has a delay feature (handy when you need to catch a snapshot of open menus and such, that close when they are not selected)
* Has a few _modes_ for snipping (rectangle, free-form, full-screen, window)
* Copy the snapshot to your clipboard, paste it anywhere

## Snip & Sketch (new)
It's the new and imporoved __snipping tool__, so it has everything the old tool has, and then some. 

* It has rulers for your pen tools, you can rotate them with the __scroll wheel__
* Crop your snip to size
* Freeform snip! Don't have to do it square
* It actually has a undo/redo
* Keyboard shortcut! `win` + `shift` + `s`. 
    > Instant access to take a snapshot, it saves to your clipboard to be pasted anywhere.


![Snip and Sketch](./images/snip-sketch.png)