# Programs
> These are notes about specific programs that might help me get back into using them in some cases.
Programs and systems

## Editors (IDE)
* [Visual Studio Code](./vscode.md)
    >A nice code editor that can be anything, when you find the right extentions to install. 
* IntelliJ (for java)
    >IDE for coding in java
* PyCharm (for python)
    >IDE for coding in python
* TexMaker (for latex/pdf)
    >A very nice LaTeX text editor, if you want things to be local (opposed to the in-browser overleaf). It has a nice tray showing all the symbols you might want to use when writing equations or scientific reports.
    >* More about this editor under _LaTeX_ in _Markup-languages_
* [Jupyter](jupyter/readme.md) 
    >A nice in-browser notbook system that allows code snippets to be executed and show the output.
* [Geany](geany-ide.md) 
    >Light-weight code editor, great on raspberryPi
## Tools and systems
* PostMan (HTTP requests/responses)
    >Sending and testing HTTP requests
* [Docker](docker/README.md)
    >Containers and virtual machines!
* [Snipping-tool](snipping-tool.md) (windows)
    >It's amazing how such simple tool can be so handy and overlooked. I prefer the old version, the new one seems bloated enough to be a lot slower.