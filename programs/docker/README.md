# Docker!
Super useful tool that is everywhere on hosting services these days, because it can easily setup an enviorment for your application or website, and easily dispose of that enviorment when not needed. Takes alot less space than having multiple computers (even they are usually virtual).

I will be using the command line to manage docker, but I do use the _Docker desktop_ to have a nice overview of what's going on in docker, since it shows the containers, images and volumes that docker has. Which makes it a easy place to stop, start or delete any of those from docker.


## Index
* ### [Basics (setup)](docker-setup.md)
    * [My experience on Win10 home and pro](docker-win10-experience.md)
        >Setting up docker has always been a hazzle for me
    * [Docker commands](docker-commands.md)
* ### [Docker basics](docker-basics.md)
    >A general overview of using docker, the links below give a closer look at things.
    * ### [Dockerfiles](docker-dockerfile.md)
        >Dockerfile produces a docker image when you build them. It is the recipe/blueprint for a docker image
        * Docker layers
            >`RUN`, `COPY` and `ADD` creates layers in the dockerfile
        * Multi-stage builds
            >Multi-stage builds use multiple `FROM` statements in the dockerfile
    * ### [Docker images](docker-images.md)
        >Images are instructions for the container how to run, you simply run the image and get a container.
    * ### Docker container
        >Containers are a running instance of a docker image. Multiple containers can be based on the same image. Cannot remove an image that has a running container based on it
* ### Container lifecycle
    >A container is created, where it can be either _paused_ or _up_ (running). A container then can be _exited_ (stopped running, can be restarted)
* ### [Docker Compose](docker-compose.md) (`yaml`)
    > Makes running containers in development so much easier
* ### [Docker ignore](docker-ignore.md)
    >What files should docker not copy over
* ### Docker Volumes
    >Persistent data for containers
    * Named Volumes
