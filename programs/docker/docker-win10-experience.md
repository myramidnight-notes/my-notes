
# My experience setting up docker
Previous troubles were on my laptop, which runs the _Home_ edition. You can read some of my notes on my experience getting docker to run and how in the end I was unable to undo the changes without factory resetting the machine.

I will still leave my little notes about the Home edition, since I wasted alot of time on that. 

>__Basically:__ You shouldn't have to change anything in the BIOS to get an application like docker to work. Just assume if no guide can help you reach your goal without playing with the BIOS. They don't want to you to use _Hyper-V_ on the _Home edition_, so you essentially can't use Docker then.

## Running docker on Win10 home edition
>__This is not a guide or tutorial, just notes I took along the way__. Long story short: __You don't enable Hyper-V on the _home_ edition__. Do at your own risk, it worked seemingly fine for a while for me, until I wanted to remove docker.

First you need to install docker. Docker desktop is quite nice when you're running things on Windows OS.

I discovered that if you are using Windows10 home edition, then you have to jump extra hoops to get enable virtualization, which docker uses to create the virtual containers. It wants to use `Hyper-V` by default, but that's not an option for the home edition. You'll have to look into something called `WSL2` and enable `Virtal Machine Platform` in your windows features. 

Perhaps I just had to enable the `Virtualization` first, perhaps the whole `WSL2` installation was not needed. But I had to restart my computer in BIOS mode to manually enable the `Intel Virtual Tech` just to get the Virtualization enabled.
* You might notice a `Vmmem` among your running applications in task manager, that's the virtual machine that docker uses.

> In hindsight: Later on I was starting to have some issues with the system, which I traced back to my changes in the BIOS, and simply wanted to remove docker and undo the changes. This lead me to discover that somehow my access to the BIOS settings was gone, I could not enter the BIOS to undo the single setting change (enabling _Hyper-V_) that I did.
>
>At least it kept my files, so I didn't loose any contents from the reset.

## My Second installation experience:
This time around, I have a Win10 Pro installed on my desktop. Let's see how smoothly things go.

1. I went with installing the __Docker Desktop__ for Windows.
    * The installer configuration offered to install the required Windows components for _WSL 2_
1. After restarting, I was greeted with this
    ![docker-initial-install](images/docker-first-install.png)
    >"Hardware assisted virtualization and data execution protection must be enabled in the BIOS"
    >* [Docker troubleshoot for Virtualization](https://docs.docker.com/desktop/windows/troubleshoot/#virtualization)
1. I just decided to __Quit__ and finish getting the virtualization working first. 
    >Can see in the task manager under _performance_ if virtualization is enabled, and if it supports Hyper-V if disabled

1. I used the [Microsoft quickstart guide to _Hyper-V_](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v) and enabled it through powershell terminal according to guide:
    ```
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
    ```
1. After restart I went to the __windows features__ panel, and finally found the `Hyper-V` option checked. Without having to manually enter the BIOS, thank goodness.
    * Apparently the task manager showed that Virtualization is still disabled even if Hyper-V is in place. Just need to enable that. But that seems to want me to mess with the BIOS. Previously I was enabling it on a Win10 Home edition, which didn't have Hyper-V in place, which caused issues.
1. So I shall enable virtualization, my computer is support to support this officially this time around from what I gather.
    * [How to enable Virtualization on Win10](https://mashtips.com/enable-virtualization-windows-10/)
    > Every bios might look different, it's based on the motherboard. I have a Auros. It named it's virtualization SVM (for Secure Virtual Machine Mode)
    > 1. Advanced CPU Settings > SVM Mode > Enable
1. After enabling virtulazation of the CPU and restarting, the task manager finally showed _Virtualization: Enabled_. So of course I finally tired running the Docker desktop again, third time's the charm, right?
    ![](images/docker-fail.png)
    >Maybe I should reinstall it, now that I got everything in place... (didn't work). 
1. There was a [fix of removing Docker from the AppData](https://thatnavguy.wordpress.com/2021/08/17/docker-desktop-failed-to-initialize/) before tying to run the program again (I simply reinstalled before trying again after removing the folders). It seems to work, I got the licence agreement to pop up.
    * But the WSL2 seems to have failed me
        >![](images/install-wsl2-incomplete.png)

1. I continued through that error, and opened docker (which was crying over how it wasn't working), went into __general settings__ and unchecked `Use the WSL 2 based engine` and restarted the program. TADA, it stopped complaining. 
    > I find it odd that it cries over the WSL2 as it offered to  "install the required Windows components for _WSL 2_". That sucks.

So that's how I got docker desktop to work. Windows wanted to update itself along the way, don't know how often I've had to restart the computer by this point.