# Dockerignore
just like git has `.gitignore`, docker can have `.dockerignore`, which will specify what files docker should not be able to copy over when creating images.