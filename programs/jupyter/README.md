# Jupyter
It is a nice open source browser based notebook program that allows you to merge together code and __markdown__ notes. It runs on a python server.

#### Kernels for various languages
>__Kernels__ are programming language specific processes that run independently and interact with the Jupyter applications and their user interfaces. The default is of course the python kernel, since Juypter notebooks are run on a python server.
>
>So you can make notes about other languages, just need to add the kernels for them (and of course, remember to swap to the correct kernels)


#### LaTeX in Markdown
> The best thing about jupyter notebooks that I've discovered, is not only the ability to document code that can actually run, but the __markdown__ cells support __LaTeX__! just place it inside `$$`

#### Running the notebooks with docker
>And jusing docker-compose makes easy work of having the notebooks everywhere and run them with a simple command `docker-compose up`.

## Index
* ### [Jupyter lab](jupyter-lab.md)
    >* Jupyter-lab vs classic notebook
    >* Kernels, Packages/Modules and new files
* ### [Installing and running Jupyter notebooks](jupyter-install.md)
    > How to install and run a jupyter notebook installation
    * [Language Kernels](jupyter-kernels.md)
* ### [Running Jupyter notebooks in Docker](jupyter-docker.md)
    > This is such a nice option when you are able to run docker
* ### [Basics: Using jupyter notebook](jupyter-basic.md)
    > The c

