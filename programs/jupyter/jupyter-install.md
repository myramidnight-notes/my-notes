
# Installing Jupyter Notebook and running
You can find documentation about installation on [__jupyter.org__](jupyter.org). 

>### PIP
> I personally went with `pip`, and even wrapped it in a `venv` (virtual enviornment) in case existing global packages would cause conflicts.

>### Conda
> If you go with installing with _anaconda_, then it might be good to know that they have __miniconda__: a version that doesn't include all the popular packages, so you can just fetch the packages you need manually. Less bloated with things you might never use.
>* [Conda installation guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)

## Troubleshoot: Kernel connection
>Because of this particular issue I ran into while trying to make Jupyter work on my _Windows10_ machine, it would constantly fail to connect to a _kernel_ when it wanted to execute a code block. No matter how I installed (with `pip`, in a `venv` or even with `conda`)
>
> The issue was solved by simply opening Jupyter in a different browser.

## Starting Jupyter
You should start by navigating to the directory you wish your jupyter server to run in through the terminal. 

They say you should be able to start up the jupyter server with the following commands

```
jupyter-lab
```
but for some reason if that doesn't work, you can open it through python as a module 
```
python -m jupyterlab
```
### Opening Jupyter notebooks
Now that you have a server running in the terminal, you can open the notebook in browser. In the terminal you can actually find a link (which you can `ctrl` click it to open directly) which has an access token attached to it for security. By default it shows you the files in the directory where you started up your Jupyter server

I actually like keeping my jupyter notebooks together, sharing a parent directory (and then opening the server in that parent directory). It just makes everything so much simpler.


## Options
### Disable automatically opening in browser
> perhaps you wish to decide which browser to open the notebook in by copy-paste the link. Just add `--no-browser` as you start the jupyter server, and it will not automatically open the notebook in your default browser. It will provide you with the links to connect to the notebook as always. Following code snippet shows how the command would look like for both `jupyter-lab` and the classic notebook.
>```
>jupyter-lab --no-browser
>jupyter notebook --no-browser
>```