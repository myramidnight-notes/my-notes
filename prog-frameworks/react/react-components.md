# React Components
Everything in the React app is composed of components, blocks that we piece together. Some blocks are even nested within each other.

### Two ways of implementing components
There are actually two ways to create components, and even though their purpose and behaviour is essentially the same, even treaded as the exact same thing, their implementation is quite different in some areas (it's the syntax, so to speak).

Knowing the difference between them, it actually becomes easier to transform them from one type to another. It's always best to stay consistant when programming, and just stick to one type or the other. 

* #### Class components
    > it uses javascript classes to implement the component
    >* We can create local state within class compoments
    >* Everything that will be rendered has to be wrapped in `render` method.
    ```js
    class MyComponent extends React.Component{
        render(){
            const myProp = this.props.myProp
            return( //The render
                <Text>My Component</Text>
            );
        }
    }
    ```
* #### Function Components
    > as the name suggest, these components are created as functions. These implementations allow you to use the magical __hooks__ that are a relatively new feature of React. 
    ```js
    // the "...props" is a wildcard for any extra props passed
    const MyComponent = ({myProp, ...props})=>{
        return(
            <Text>My Component</Text>
        );
    }
    ```

Just looking at the code examples of a very raw component in either implementation, it becomes obvious that function components have simpler syntax, and with the arrival of __hooks__, we can actually do everything with only function components (state was impossible for function components previously).
## Structuring components
The simplest trick to plan your components is to have a __visual representation of the application and it's pages__ (can be a sketch or even presentation in Figma). This gives you the best overview of all the possible elements that the application will be composed of
* What elements is the app composed of?
* What elements are reusable?
* What elements are children of another element?

It can be slow work to start coding without a plan, and knowing what components you are going to need to create is a good starting plan. Things don't even have to be pretty when you start out, because we mainly want things to appear in correct places and see if they behave as expected.

### Organizing the components
Each component will be it's own file, and since javascript allows you to be very flexible with the way you implement things, there are various ways to go about actually creating the components.

* We could have a directory dedicated for each component, keeping all the child components in the same directory. 
    * naming the component file `index` will actually allow us to just refer to it by the directory (instead having to point to the file), because `index` tells the program that this is the center file for this directory.
    * some people like keeping stylesheets for individual components, storing them within the associated directory.
* We could implement the child-components __within the parent component file__, specially if those child components will never be seen outside of the parent.

It is handy to have a rule of thumb, that it's worth breaking a component into it's sub-components when it starts growing complex. We then simply import them. 