# React Web App
We can also create webapps using React, it doesn't have to be for native platforms.

## Initializing react app
There seems to have been slight changes to how we can use `create-react-app` since the last time I used it. Instead of having to install the tool, we can just run it.
* [Create React App](https://reactjs.org/docs/create-a-new-react-app.html)
    ```
    npx create-react-app my-app
    ```
    * `npx` seems to be a _package runner tool_ that comes with `npm 5.2+`
    * When it's done setting up your project, you have to move into the newly created directory (`my-app`) and run your server from there with the provided commands. (usualy `yarn start `)
    * The terminal window running the server will notice whenever you save any changes, and lets you know when it's done building the app (so that the changes will appear when you refresh) 