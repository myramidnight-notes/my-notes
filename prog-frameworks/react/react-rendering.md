# Rendering the app on screen
React uses __JSX__ for rendering, which is a html representation of the javascript code.

## SPA (Single Page Applications)
__SPA__ is centered around how the whole application is technically just a single page that changes with DOM manipulation by the Javascript.

If __DOM__ is an abstraction of the HTML document, then __SPA__ is an abstraction of the DOM. __DOM__ stands for _Document Object Model_, which is the tree structure that is generated from the HTML document. 

### Virtual DOM
SPA is a __virtual DOM__ that was created because manipulating the actual DOM is quite slow and has performance issues. So a simplified copy is created that allows the application to do calculations on the DOM to check if it actually has to change anything in DOM or not, because __re-rendering everything is expensive__ when considering performance. We just want to change the relevant elements, and only when needed.