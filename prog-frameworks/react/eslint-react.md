# ESLint for react project

### Add packages 
* `eslint`
* `babel-eslint`
* `eslint-plugin-react`

### Initialize
You can copy your favorite rules over after it has been initialzed.
```
eslint --init
```
> It will ask you a few questions about your project and what rules you want to apply
## Set the version of react
ESLint really wants to know what version of react you are using, so just add the following snip to the `.eslintrc.js` so it will stop complaining and just detect the version.
```
"settings":{
    "react": {
        "version": "detect"
    }
},
```