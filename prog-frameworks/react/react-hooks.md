# React hooks
There is a relatively new feature in react which is called __hooks__, that makes using anything that could have a state of any sort (meaning it needs to remember things) so much easier to implement. 

Apparently hooks can __only be used within function components__! 
If you are thinking "_but I need class components to define a component state!_", do not worry, because there are hooks for those too! 

>Hooks are wonderful and they could even phase out the need for __class components__ in favor of __function components__. Which I don't mind, _classes are weird_ (they seem so un-javascripty and confusing).

I will be focusing on hooks that come with React. But most packages that are made to work with React seem to be making use of hooks as well. Just remember that you have to import them from react.
## Effect hook 
The `useEffect()` hook is similar to `componentDidMount` and `componentDidUpdate` (which can only be used in _class components_). If you are unfamiliar with `componentDidMount` and `componentDidUpdate`, then here is a little explaination:
* `componentDidMount` 
    > a method that will only run the code within once the component is done mounting (ready to be rendered). This would be code that only runs at the initial render of the page.
* `componentDidUpdate`
    > a method will run the code within when the component updates/re-renders. 
    >
    > This seems to be the __default behaviour of `useEffect`__

So you simply place your code within a __callback__ in `useEffect` (the first argument)

#### Disable the `componentDidUpdate` behaviour of `useEffect`
>Perhaps you want the `componentDidMount` behaviour instead of `componentDidUpdate`. There is a little trick for that:  __add a empty array as the second argument__.
>    ```js
>    useEffect(()=>{
>        //your code
>    },[])
>    ```
>This will have the effect of only running the enclosed code when the page is initially loaded. 

## useState hook
Here you have your component state! Information that the component wants to remember. 

If you are familiar with how __class components__ created the state, where it was stored as a single object with any number of properties.

When you create a state with hooks, you actually define each state property individually, as well as the method used to change that state property.

```js
[myState, setState] = useState([])
```
* The `useState` returns two things that will be mapped onto the variables you define in that array.
* within `useState` you tell it what the initial state of that property is. It can be an array, string, number... anything really.

#### Adding things to array in state
>The thing about React, it is really pick about how you update a state, because it might just ignore what you change when you simply overwrite values that it's keeping track of, such as arrays that it uses to render components with. So if you wanted to add a single item to an array, you don't push to it. 
>```js
>// Correct way to update an  array in state
>setState(arr => [...arr, newItem])
>```
>* What's happening here? we are taking the current state of this property (nicknamed `arr`), and cloning it into a new array with the new item included.
>* This is how you make React notice that you actually changed the data, else it might ignore whatever you did to the data.