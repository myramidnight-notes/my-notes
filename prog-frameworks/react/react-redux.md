# React with Redux
Redux gives you a global state called `store`, which can be accessed throughout the app. This is where you keep the data that is relevant to the whole application.

Redux is a implementation of a Flux Design Pattern, that consists of action, dispatcher, store and view.

React native components become simpler to handle because they no longer have to manage a state.

## Actions
Actions contain a type property as well as a payload, which is the data that is delivered to the store.
* Decrement
* Increment

### `redux-thunk`
It is a middleware that lets us return functions instead of javascript objects. Thunks can be3 used to delay the dispatch of an action or only dispatch if certain criteria is met

ReduxThunk injects the dispatch function as the first parameter to the function returned from the asynchronus action creator.