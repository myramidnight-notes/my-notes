# Input and Forms

## React Hook Form
Apparently there is a package called [React Hook Form](https://react-hook-form.com/) to make your life easier when it comes to creating forms.
```
npm install react-hook-form
```
> It has a seperate guide to how to use the package with `react-native` specially. 
## Numbered input
If you wish the `TextInput` to only accept numbers, then you can give it the following property
```js
<TextInput 
    keyboardType={"numeric"}
/>
```

## Warning / Help message
If you are using the `react-native-paper` package for your style, then it provides the `HelperText` component to render these messages, and hide them based on conditions (such as reminding people about required fields).

## React and Hooks.
Apparently react has `hooks` now, that allow you to do things in function components that previously was only possible in class components, and the hooks even make it all alot simpler.