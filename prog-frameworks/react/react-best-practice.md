# Best practices
Some tips and tricks I picked up
>* [__PropTypes__](#PropTypes)
>* [__Context provider__](#Context-provider)
>* [__Children props__](#Children-props)
>* [__Error Boundry Class__](#Error-boundary-class)
## PropTypes
A way to allow us to define types for props.
1. Import `prop-types`
1. Define the prop types
    ```js
    import PropTypes from 'prop-types';

    const myComponent = ({prop1})=>{
        return(
            <h1>{prop1}</h1>
        )
    };

    myComponent.propTypes = {
        prop1: PropTypes.string
    };

    export default myComponent;
    ```

## Context provider
Sometimes we need to expose something  _globally_, to use it in multiple components, but it can be difficult to get the information down through a chain of components by passing it as props.

* Logged in user info
* Theme settings
* ... something that will be used in multiple places

In order to avoid having to pass props through multiple components, we can use something called __context__. It handles passing down the context to child components.

You might have noticed a _provider_ component (in _redux_ or _router_) that is wrapped around the parent component of our app (`App.js` for example). This is for providing context to all the children.

### Creating context
1. Create a context using `React.createContext()`
    
    ```js
    import React from 'react';

    const defaultValue = {
        // some initial values
    }

    const MyContext = React.createContext(defaultValue);
    ```
    * It returns a context, and accepts a single parameter which is the _defaultValue_.
    * The default value is only used if there is no parent context above it in the hierarchy.
 
1. Wrap the JSX in __Provider__ component: `<MyContext.Provider />` 
    ```js
    const contextValue = {
        user: {
            id: 1,
            userName: 'mrmiyagi',
            sessionToken: 'afeiapwfeðawiefjðw'
        }
    };

    <MyContext.Provider value={ contextValue }> 
        <App />
    </MyContext.Provider>
    ```
    * Here we are supplying `contextValue` instead of the `defaultValue`

### Context consumer
```js
<MyContext.Consumer>
    { value => <div>{value.user.userName}</div>}
</MyContext.Consumer>
```

## Children props
The __children__ prop allows us to pass child nodes to the component.
```js
export default const ContainerComponent = ({children})=>{
    return(
        <div className="container">
            {children}
        </div>
    )
}
```
And then we can pass it the children, and they will render within the parent.
```js
<ContainerComponent> 
    <ChildComponent />
    <ChildComponent />
</ContainerComponent>
```

## Error boundary class
__Error boundries__ are React components that catch _JavaScript_ errors anywhere in their child component tree, log those errors and display a fallback UI. 

They however do __not__ catch errors for the following:
* Event handlers
* Async code
* Server side rendering
* Errors thrown in the error boundry itself

### Creating a error boundary class
A class component needs to define the following in order to become a _error boundary class_:
* `static getDerivedStateFromError()`
    > Displays a fallback UI
* `componentDidCatch()`
    > Log error information

These components work like a __try catch__ block for their child components. 

### Example of error boundary class
>```js
>import React from 'react';
>
>class ErrorBoundaryClass extends React.Component {
>    constructor(props){
>        super(props);
>        this.state = {
>            hasError: false,
>            errorMessage: ''
>        };
>    }
>    static getDerivedStateFromError(error) {
>        this.setState({
>            hasError: true,
>            errorMessage: error
>        });
>    }
>    componentDidCatch(error, info){
>        console.log(error, info);
>    }
>    render(){
>        if (this.state.hasError){
>            return <Error errorMessage={ this.state.errorMessage} />;
>        }
>        return this.props.children;
>    }
>}
>
>export default ErrorBoundaryClass;
>```
>Then you just wrap this component around the children.

> #### Useful info about Error Boundaries
>* Error boundaries only catch errors in the components below them in the tree
>* They do not catch errors thrown within themselves (within the error boundary class)
>* If an error boundary fails, it will propagate to the closest error boundary above it.

## JSX fragments
We always have to have a element that encapsulates the JSX, which is the DOM elements that are returned/rendered by the component.

The problem with this is the extra pointless `<div>` elements that are used just to meet this requirement of JSX. But we can actually use something called `React.Fragment` instead of these extra divs, so that we dont' have to return extra nodes(elements).

* The __fragment__ is not actually rendered as an element, and only serves the purpose of encapsulating the JSX for React. Which allows us to avoid having a _div soup_ just to meet these requirements.

### Example of unfragmented and fragmented
* Unfragmented
    >```js
    >const UnfragmentedComponent = ()=>{
    >    return(
    >        <div>
    >            <h1>Welcome</h1>
    >            <p>I hope you find something useful here</p>
    >        </div>
    >    )
    >}
    >```
* Fragmented
    >```js
    >const FragmentedComponent = ()=>{
    >    return(
    >        <React.Fragment>
    >            <h1>Welcome</h1>
    >            <p>I hope you find something useful here</p>
    >        </React.Fragment>
    >    )
    >}
    >```
* Fragmented: minimal
    >```js
    >const FragmentedComponent = ()=>{
    >    return(
    >        <>
    >            <h1>Welcome</h1>
    >            <p>I hope you find something useful here</p>
    >        </>
    >    )
    >}
    >```