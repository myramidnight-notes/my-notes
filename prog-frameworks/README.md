# Frameworks
Why re-invent the wheel when someone else has the blue-prints for it? This also applies to features and functionality, why implement somtehing from scratch when there is a library for it? 

## Programming frameworks

These are frameworks created to make programming easier in various fields. This list will only contain frameworks that I've actually touched upon. If there are notes missing, then it usually means I haven't gone deep into it.

* ### C#
    * [__Dotnet (`.NET`)__](dotnet/README.md)
        >Very nice for a layered programming and large projects.

* ### JavaScript
    * [__React__](react/README.md)
        >It is a __web framework__ that makes creation of interfaces or UI components very easy.

* ### Python
    * __Django__
        > It is a high-level python __web framework__

* ### PHP
    * __Laravel__
    * __Symfony__

## Content Managment Systems (CMS)
Content managment systems are generally used for managing web-content or enterprie, but I am only familiar with the web aspect of.

We might not always consider CMS to be _frameworks_ in the same sense as the list above, but more of a __platform__ or software, depending on the system.

Often the main focus in web-development is the frontend, what people see, and that makes it very optimal to just use existing frameworks/platforms/systems under the hood and just spend the time and energy developing that frontend. 

* ### PHP
    * WordPress (PHP)
        > Specially designed for creating blogs and news sites. Very customizable.
        >* [Wordpress.org](https://wordpress.org/)
    * Drupal (PHP)
        > A very flexible content managment system, it can create blogs, shops, anything really. Seems to be the preferred and more professional choice for websites built for institutions and goverment (at least here in Iceland). 
        >* [Drupal.org](https://www.drupal.org/)
    * MediaWiki (PHP)
        > For creating information/documentation sites, such as Wikipedia. They can be specialized with extentions. They have their own markup language, called `wikitext`. Has inbuilt version control (git) to track all changes to the content.
        >* [Mediawiki.org](https://www.mediawiki.org/wiki/MediaWiki)
    * phpBB forum
        > A very popular and classic forum system for apache servers, though more dynamic forum systems have been gaining popularity, such as _Discorse_
        >* [phpBB.com](https://www.phpbb.com/)
    * Piwigo
        > It is a very nice open-source gallery software
        >* [Piwigo.org](https://www.piwigo.org/)