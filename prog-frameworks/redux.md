## CreateStore
This is an example of how the `createStore` function works, with the state as a integer variable. It can be (and usually is) a typical javascript Object.
```js
function createStore(reducer){
    let state = 0;

    const getState = ()=>(state);

    const dispatch = (action)=>{
        state = reducer(state,action);
    }

    return { getState, dispatch };
}

const store = createStore(reducer);

const incrementAction = {
    type: 'INCREMENT',
    amount: 3
};

store.dispatch(incrementAction);
```