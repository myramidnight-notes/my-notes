# Using SQL with EF in your dotnet project
> Dotnet 3.1

We made use of the `Microsoft.EntityFrameworkCore` to create database context and create SQL migrations based on the entity models, and `Npgsql.EntityFrameworkCore.PostgreSQL` to make use of PostgreSQL in our project.


## The connection string (authentication to database)
Add the following fields to your `appsettings.json` and fill with the usual information. Database and User ID are the same thing.
```json
  "ConnectionStrings": {
    "MyDatabase": "Server=<Server>;Port=5432;Database=<DbUser>;User ID=<DbUser>;Password=<YourPassword>;"
  }
```
## Entity Models (Schemas)
You do have to add reference to any other tables/Entities, using lists to indcate a `one-to-many` or `many-to-many` relationships. These references are used when you establish the database context.
```c#
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyApplication.Software.API.Models.EntityModels
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        // relations with other tables
        public List<PaymentCard> PaymentCards { get; set; }

    }
}

//And the PaymentCard Entity 
namespace MyApplication.Software.API.Models.EntityModels
{
    public class PaymentCard
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CardholderName { get; set; }
        public string CardNumber { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        // relations with other tables
        public User User { get; set; }
    }
}
```
## Setting up the Database Context
Need to create your database context, perhaps in a folder in the Repository layer called `Contexts`.
```c#
using Microsoft.EntityFrameworkCore;
using MyApplication.Software.API.Models.EntityModels; //The Entities

namespace MyApplication.Software.API.Repositories.Contexts
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)  {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //here you define the table relations and foreign keys 
            modelBuilder.Entity<PaymentCard>()
                .HasOne( c => c.User)
                .WithMany( u => u.PaymentCards)
                .HasForeignKey(c => c.UserId);
            modelBuilder.Entity<User>()
                .HasMany(u => u.PaymentCards)
                .WithOne(c => c.User);
        }

        // database sets, they link the Entity model to a database table, set one for each table.
        public DbSet<User> Users { get; set; }
        public DbSet<PaymentCard> PaymentCards { get; set; }

    }
}
```

## Configuring your database context
You need the following imports
```c#
using Microsoft.EntityFrameworkCore; //gives you AddDbContext
using System.Reflection; //Gives you MigrationAssembly
```
and add the database context to `ConfigureServices`
```c#
    services.AddDbContext<MyDbContext>( options => {
        options.UseNpgsql(Configuration.GetConnectionString("MyDatabase"), options => {
            options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
        });
    });
```
> `MyDbContext` is class you created as a DatabaseContext, `MyDatabase` is the key of the connection string within `appsettings.json`

## Using the EF tools
You might need to add the EF package specially to the dotnet if you can't run the `dotnet ef` commands. 
```
dotnet add package Microsoft.EntityFrameworkCore
```
When you've finished setting up your Entities and datbase context, then you can create migrations. This will translate whatever you created into something that can interact with the SQL database.

After the migration has been created, then you can use the `datbase update` feature to use those migration files to update the database and create the tables and relationships.

#### Create Migrations
```
dotnet ef migrations add MigrationName
```
#### Update the database with migrations
```
dotnet ef database update
```