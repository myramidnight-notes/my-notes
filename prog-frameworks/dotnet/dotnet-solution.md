# Solution files in dotnet projects
A solution is a structure for organizing project. It is like a container for the project, and comes in handy when you are working on your application out of IDE.

### uses of solution file
>1. Open project or multiple projects in IDE just by opening solution file in IDE. If you open solution file in _visual studio_ then all of the projects which are part of solution will get openend in _visual studio_
>1. You can control build/deployment of projects (what to build and what not to) under solution using solution level configuration
>1. If you are using editor like VSCode, then solution file facilitate to build all those projects which are part of the solution just by running `dotnet build` command. If projects were not part of solution then you need to build them individually
>1. Whenever working out of IDE (like _Visual Studio_) and source control you can add all project files and solution to source control just by adding solution to source control. Same goes with checkin or checkout.
>
>These are the advantages of solution file and there can be many more. But it is not a must-have thing, it just makes your life as a developer easier. -[stack overflow](https://stackoverflow.com/questions/43426982/dotnet-core-purpose-of-solution-files)

### VSCode extention: `vscode-solution-explorer`
>It simply gives you a new tab which will be an explorer for the project based on the solution file in the root of the work directory.
>
>![](images/vscode-solution-explorer.png)