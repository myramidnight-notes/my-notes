> This is part of [Glósubankinn](https://gitlab.com/myramidnight-notes) 

These notes were initially compiled during my time studying _Computer Science_ at the __[Reykjavik University](https://www.ru.is/)__. I have graduated with a BSc and a fancy work title I can use, but since you never stop learning (besides for the fact that programming is a field that constantly evolves) I will continue to add to these notes as needed. 

The local language is Icelandic which might lead to some weird wordings or things lost in translation in these notes.

I am such a __documentation nerd__, so of course I had to create a collection of information like this, even if it's just for myself.

### Best way to learn is to explain things to others
> I learned that it's called _the Feynmann Technique_, to learn by explaining the concepts to others. Identifying gaps in your explanations to find what you need to understand better. If you can properly explain a concept to others, then you understand it yourself and it cements the knowledge even better within your head. 
>
> Write it down as if you're teaching it to a child or anyone really. Write everything you know about the subject in a way that explains things and makes them understand what you are talking about. In a way it could even teach yourself if you happen to forget the details of the subject later on. The more effort you put into your notes and to really understand what you're explaining, the better the information will stick.
> 
> And when you improve your notes, you improve your knowledge, review what you've explained, identify if you misunderstood something or missing some information to properly explain something.

### Constantly evolving
>Please keep in mind that these notes are constantly evolving while I learn the subjects, correcting something that was incorrect or adding improvements to prevent confusion. I even have a habit of completely re-arranging the folder structure if needed, which would break any links if you happen to bookmark anything. 
>
>#### How I use the Double-quoteblocks
>> I've started to use double-quoteblocks (like this one, with the double vertical lines) if the text is straight from the source. Mainly keeping it so that the notes don't have holes in them, until I get the chance to re-write them in my own way (because if they haven't been re-written, then I haven't actually processed, more risk of it getting forgotten sooner than if I had bothered to explain existing texts in my own way)
>>
>> I use these double-quoteblocks also to keep previous versions of note sections, when I'm in the process of improving the notes by re-writing them again. It's just how I work.

### Navigating through the notes
> I make use of the `README` files within each directory, because of the neat feature of their content being automatically displayed when viewing these directories on _gitlab_ directly. These might contain the most accurate indexes for sub-files. 
>
> I try to avoid detailed indexes in upper level directories, mainly directing to the next README in question. This removes the need to update so many files if I happen to want to change the structure of things (wich can happen on a whim).

# Index
* ### [__Command line interface (CLI)__](./cli/cli.md) / Terminal
    > The very basic interface to give your computer commands. There exist many fancy graphical interfaces (GUI) that make it nice to interact with, but sometimes you just need to go basic.
    >* [VIM text editor](./cli/cli-vim.md)
* ### [Git and Repositories](./git/README.md)
    > Version control, project colaboration and backups with online repositories (such as GitHub and GitLab). Very handy when working with code and digital documents.
* ### [Programs and Text Editors](./programs/README.md)
    > There are many programs out there to improve the working enviorment for programmers. These are tools and code editors, and even a few IDE that I've had to use. These notes help me recall some of their handier features.
* ### [Markup languages](./markup-languages/README.md)
    > Languages that provide syntax for formatting and styling content. They are not always for programming or the web, some are for creation of documents.
    >* HTML, CSS, PUML, LaTeX, WikiText...
* ### [Programming Languages](./prog-languages/README.md)
    > Various coding languages that have been developed to allow programmers to communicate with the hardware to produce results
* ### [Programming Frameworks](./prog-frameworks/README.md)
    > Frameworks to make creation of applications/programs quicker and easier for programmers, not having to build everything from scratch.
* ### [Web services](./webservices/README.md)
    > Servers, Content Managment Systems, Docker...
* ### [Databases](./data-storage/README.md)
    > Some notes about various database systems and query languages
* ### [Web development](./webdev/README.md)
    > Things that are relevant to web development but not technically programming. 