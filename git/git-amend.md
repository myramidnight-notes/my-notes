# Git Amend
When you want to add something to a previous commit, such as when you forgot a little thing and don't want to create a whole new entry in the commit history
* [Rewriting history](https://www.atlassian.com/git/tutorials/rewriting-history)


To add to previous commit, we are actually creating a new commit, that includes the previous commit. That is why it's part of commit, and you write a new message.
```
git commit --amend -m "The new message for the commit"
```
instead of 
```
git commit -m "message describing changes"
```