# Git cached
If a subdirectory in your git project happened to have it's own git initiated, then you have to do something extra before you can commit the whole project. If you do not do anything about it, then it will technically ignore anything in the sub-git project, since it expects it to be managing it's own git within.

This can happen if you are using clients to initiate your programming projects for you and they might already have a git initiated. If you are trying to contain multiple such projects within one git repository, then you will need to handle those sub repositories somehow.

You will be notified if a subdirectory has git initiated, and if you push it without handling it, then the subdirectory will not be accessable within the online git repository. 

* You can add the sub-git project as a submodule (I have not tried that)

* Or you can remove the `.git` from the subdirectory (which is a hidden file, try viewing hidden files within the file explorer).
    * After you delete the `.git` folder from the sub directory, then you can remove it's cache.
    ```
    git rm --cached subDirectory
    ```
    * After that you can add and commit the subdirectory as expected.