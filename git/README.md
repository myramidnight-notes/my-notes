* [Detailed basic guide](notes-git.md)
    > Commiting, Pusing, Branching, Basics...
* [Combining nested repositories (cached git)](cached-git.md)
    > Because sometimes projects already have git initiated
* [Git Archive](git-archive.md)
    >Nice and simple way to compress/zip up your project without all the files you ignore.

## Why should I use `git`?
> Because you can't always depend on `undo` to save you. 

Git is __version control__, and has become an essential tool in any project that relies on digital files. It could be a programming project, writing a book, or anything else saved on a computer. You can save the state of your project as a `commit` and use it to compare versions or revert to a previous version of the project.

### Do you find `git` confusing/intimidating?
Git is very simple at it's core, you just `add` your changes to a stage, and then `commit` all the the files you added. That's all it takes to save a version of your project. Add a `push` and `pull` to the list of commands if using a _remote repository_ and you got the basics down!

Perhaps you simply find it intimidating to use the _terminal_ (CLI), all the possible commands if you want to dive into the more advanced features of git. But there exist tools that build ontop of git to make your experience even easier. Graphical interfaces are always nice, something to visualize, or perhaps inbuilt tools in your favorite code editor. 

There are even services like Gitab or Github that can make handling your merges and using advanced features easier once you've pushed your changes to their servers.

## What does `git` do?
Git is a versioning tool, it keeps track of all changes within a project directory. It gives you such flexibility and assurance in those cases where you wish you could go back in time and get your previous version of the files.

Git works witout internet, because it just keeps track of your local projects in the form of _commits_. Each commit is a checkpoint back in time that can serve as a backlog to see how the project evolved, or to jump back in time and restore the project from a specific commit.

### Remote repositories (git servers)
There exist many online git servers, the most obvious ones might be _gitlab_ or _github_. They store your _remote repositories_ and those particular services have built many useful tools ontop of `git` to create an ideal enviorment for developers. You could also keep it all private and just store your school notes on there.

You could even host your own git server, if you wish to keep things extra private.

### Workspaces and different versions (branches)
Perhaps you are experimenting alot with your project, end up having many different versions of it. Git makes it easy to compare and combine your different versions, and let's them live seperate from each other within the same project as __branches__ while still existing in the same space. Jumping between them is quite simple. 

The _master_ branch would essentially be the base version.

## Fancy GUI tools
You can always find nice GUI that gives a nice overview and access to the various git superpowers. Though I personally prefer the commandline for interacting with git, but that doesn't mean I don't love getting a pretty overview graph of the commit history! Gitlab has such a nice graph, and I have learned that _gitkraken_ also provides such a nice overview (though only for puplic repos if you are using it for free)

But it doesn't really matter how you interact with git, it's still a indispensable tool when working with files, be it programming, composing text documents, or anything really that you wish to have the ability to go back in time and see how the files were at different points of the project. 