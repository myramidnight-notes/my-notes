# Justfile

It resembles `makefile`, and it essentially means "just running scripts". So you can create __recipes__ to call upon, creating __commands__ that run various scripts, code or commands when you use it.

* [Just](https://github.com/casey/just)