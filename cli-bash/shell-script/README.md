# Shell Scripts
Essentially `bash` scripts (the linux terminal commands).

You can actually create a script (with file excension `.sh`) that has a little program that runs bash commands.
* It can have variables, functions and conditional statements

## Links
* [Google styleguide for Shell scripts](https://google.github.io/styleguide/shellguide.html)
* [If statements  in bash scripting (tutorial)](https://ryanstutorials.net/bash-scripting-tutorial/bash-if-statements.php)
    * [Nested if/then conditoinal tests](https://tldp.org/LDP/abs/html/nestedifthen.html)
* [Check if directory exists in bash](https://www.cyberciti.biz/faq/check-if-a-directory-exists-in-linux-or-unix-shell/)
* [Activate venv in bash script](https://stackoverflow.com/questions/13122137/how-to-source-virtualenv-activate-in-a-bash-script)
