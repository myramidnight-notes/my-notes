# Managing a domain
Alot of this info comes from how my domain records are set up on my service provider.

## Host
This would be the prefix to the domain, if the domain would be `mydomain.com`, then that's what the `@` stands for (as a shorthand for this domain). 

So a host name `mail` would be translated to `mail.mydomain.com`, which is techinically a sub-domain
## Records
### A records
A records are used for conversion of domain names to corresponding IP addresses

These also include redirects, if you want a subdomain to redirect to a specific url

### Cname records
The CNAME record specifies a domain name that has to be queried in order to resolve the original DNS query. Therefore CNAME records are used for creating aliases of domain names. CNAME records are truly useful when we want to alias our domain to an external domain. In other cases we can remove CNAME records and replace them with A records and even decrease performance overhead.

### MX records
The MX resource record specifies a mail exchange server for a DNS domain name. The information is used by Simple Mail Transfer Protocol (SMTP) to route emails to proper hosts. Typically, there are more than one mail exchange server for a DNS domain and each of them have set priority.

### NS records
The NS record specifies an authoritative name server for given host.

### SRV records
A Service record (SRV record) is a specification of data in the Domain Name System defining the location, i.e. the hostname and port number, of servers for specified services.

## TTL (Time to live)
This setting tells the _DNS_ resolver how long to cache a query before requesting a new one. The information gathered is stored in the cache of the recursive or local resolver for the __TTL__ before it reaches back out to collect a new, updated details

It is set to seconds
* `900` will be 15 minutes
* `86400` will be 24 hours