# Apache web server
* I installed `apache2`
* You will probably want to allow `http` (port 80) and/or `https` (port 443) on your firewall if you have one set up (which you should... `ufw` is a good choice if you can't pick, it stands for _uncomplicated firewall_ ).



## Checking your web server
```
systemctl status apache2
```

### Commands for your apache server
```
systemctl stop apache2
systemctl start apache2
systemctl restart apache2
```
if you were just making changes to the configuration, then you just want to reload the server
```
systemctl reload apache2
```

## Checking your Configurations

### Seeing all the configurations of apache2 application
```
ls /etc/apache2/var/www/*
```
Configurations that is good to notice
* /etc/apache2/apache2.conf – The main Apache global configuration file, that includes all other configuration files.
* /etc/apache2/conf-available – stores available configurations.
* /etc/apache2/conf-enabled – contains enabled configurations.
* /etc/apache2/mods-available – contains available modules.
* /etc/apache2/mods-enabled – contains enabled modules.
* /etc/apache2/sites-available – contains configuration file for available site (virtual hosts).
* /etc/apache2/sites-enabled – contains configuration file for enabled sites (virtual hosts).


### Checking your hostname
```
hostname -I
```
You can visit that hostname/ip in a browser to check if the web server is working.

## Setting up virtual hosts (domains)
You might (and should) keep your separate websites in different directories. The default website that appears when you set up the apache server is located at
    ```
    /var/www/html
    ```

custom domains should be
    ```
    /var/www/your_domain
    ```

## Restrict access to directories from browser
Had to edit the `etc/apache2/apache2.conf` file to globally disable access to browse the directories from browser (since it can be a security risk). 

Main point was to change `Indexes` to `-Indexes`.
```
<Directory /var/www/>
	Options -Indexes +FollowSymLinks
	AllowOverride None
	Require all granted
</Directory>
```