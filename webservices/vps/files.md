# Handling files on the server

### SCP (Secure Copy)
1. Have terminal open in the directory on your computer that you wish to copy files to, from the server.
1. use `SCP` to copy files securely
    ```
    scp root@123.123.123:/var/example/logfile.txt .
    ```
    That is the secure copy command, with the username + host name/ip (`root@123.123.123`), then the directory/file you are aiming to copy `:/var/example/logfile.txt` and lastly where you want to place the copy, here `.` just means put things in the current directory, but you can of course specify a directory.
    ```
    scp -r root@123.123.123:/var/example .
    ```
    This is the secure copy with a recursive flag `-r` to make it copy the specified directory `/example` and anything that might be within it (like more directories) 