# Web services
* [Servers](servers/README.md)
    > Servers are essentially computers that accessable through the internet. A single computer can host multiple servers, each with their unique _Port_ number. 
    >
    >For example a _shared hosting_ service would have a _Apache_ server set up for managing and hosting websites.
* [Content Managment Systems (CMS)](cms/README.md)
    > Ready softwares that make content management easier for websites to deploy. Some are frameworks that allow customization of these systems. 
    >* WordPress, Drupal, MediaWiki, phpBBforums... 
* [Docker](docker.md)
    > A platform that handles virtualization of Operating Systems for servers. Each server would be deployed in _containers_ that can easily be discarded. 
* Messsage Brokers
    > Communication tool between applications/services, connecting them together a whole. This is essential for MicroServices (such as Netflix)
    * [RabbitQL](rabbitql.md)

# [VPS](vps/README.md) (Virtual Private Server)
just notes to keep things running
