# Linux on Windows
If you can use virtualization on your computer, then you can install the _Ubuntu_ app from the windows store to essentially have a linux machine (virtually) that can access the files on your windows machine. So it's not really a separate machine, just a way to interact with your files through linux.

But it is good to keep in mind that when you install the Ubuntu app (it gives you a terminal, not a GUI), it will be a clean installation, it cannot use anything that is installed on Windows. You will need to install the things you need.

### Why get Ubuntu on Windows?
* You get to virtually interact with the files in your Windows file system through the ubuntu linux terminal
* Linux comes with G++ and everything you need to program in `C` languages.
    * So you could write your code normally in your windows environment, but use the ubuntu to compile it and run.
    * Something you compile with the Ubuntu terminal will be compiled for linux, so naturally the resulting file can't be run directly on windows, but requires the Ubuntu terminal to be run as well.
* It is still a separate system, will need to install anything you need, even if it might be installed on the windows machine (The linux would not be able to use it anyway, differently compiled).

## Setting up
1. Be sure that your Windows system and CPU supports Virtualization
1. Check if virtualization is enabled
    * You can see in the task manager if virtualization is supported and/or enabled.
1. Get _Ubuntu_ from the windows store
1. Create your C++ program, such as hello world, as you would normally do on your code editor on windows. I named mine `helloworld.cpp`
    ```cpp
    // Your First C++ Program

    #include <iostream>

    int main() {
        std::cout << "Hello World!\n";
        return 0;
    }
    ```
1. Navigate to that program in the Ubuntu terminal
    > This might take some getting used to, trying to navigate your windows machine in a linux terminal
    * It has it's own home/desktop directory that it starts in by default
    * You can actually now open the Ubuntu terminal in the Windows Terminal (new tabs)
1. Compile your program through the Ubuntu terminal
    ```bash
    g++ -o helloworld helloworld.cpp
    ```
1. Run your program through the Ubuntu terminal
    ```
    ./helloworld
    ```
    * You should have seen `Hello World!` print in your Ubuntu terminal.

This is still a very quick way to get into programming with C languages and compiling them to see if they work, becaus GNU (G++) comes with any Linux installation by default, no need to install them.

But getting them to compile to windows requires GNU that works for Windows.