# LaTeX editor: TeXMaker
I decided to try out __TeXMaker_ because it was highly reccomended and popular when it comes to installed _LaTeX_ editors. 

Even after discovering that I can easily render TeX code in Visual Studio Code (my preferred code editor), I still find TeXMaker the best way to compose TeX code in. It gives such easy access to the things you are likely to use
* Fast navigation between sections with the `structure` tab (gives good overview)
* it lets you browse lists of math/science related symbols you might want to insert (found in the `structure` sidebar)
* has auto-complete suggestions when writing their code
> Just take a look at the many options on the [TeXMaker website](https://www.xm1math.net/texmaker/). Such a good cross-platform TeX editor that is also completely free.

## Setting up _TeXMaker_
1. Install [TeXMaker](https://www.xm1math.net/texmaker/) from their website.
> If you have issues with rendering pdf, then you might consider installing [MiKTeX](https://miktex.org/), it was required by _TeXMaker_ for certain things including the _PDF_ rendering of the code. Perhaps a recent update made it obsolete.
>* Noticed there were sometimes little errors about Miktex not being able to find `pearl.exe`. Installed pearl manually and things stopped complaining (even if those errors didn't break the program, could take a wild guess at it being related to the TOC being unreliable for me (table of contents) rendering.)


## Navigating the UI
The TeXMaker editor might seem a bit confusing with all the tiny icons, but it's quite easy to get used to. Here are the main things I would point out as important to know:
* In the bottom left corner are buttons to togge some of the interfaces.
> 1. the `structure` sidebar that lets you see and pick symbols
> 2. the `message/log` that shows you any errors that might pop up in the code.
> 3. the `pdf viewer` that toggles the pdf preview panel.
* The `quick Build` option to render your code.
* Just hover over any of the many icons to get a little info-text to know what it does.

![The TexMaker UI](images/latex-texmaker-ui.png)

## Adding custom auto-complete
>It can be bothersome when you add your own things, but can't make use of the nice auto-complete that the editor does for basic things. But you can actually add your own custom auto completes.
1. Go into `User`
1. There you find `Customize Completion`
1. Add your own things there, such as `\begin{definition}` (it will handle creating the end tag as well)
