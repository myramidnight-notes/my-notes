> This is also mainly to remind myself of how I structured complex latex notes
# Latex: Structuring chapters with sub sections
Sometimes you might want to include chapters, to keep things organized, but also to keep the _code_ of each chapter in separate file. This is actually pretty simple when you have a plan.

1. Main file (where everything is imported and rendered)
    >It imports sub directories and their sub sections
    >
    >It will also need to include any `usepackage` that any of the chapters and sub sections might need, in order for the `pdf` to render.
1. Chapters (each in their own sub directory)
    * Each chapter have it's own __preview__ `txt` to render only the chapter
        > Seems to be a bother to have to re-render a whole book to see changes on a single page. 
        * These preview files would include all required `usepackage` needed for rendering
        * These are only for preview purposes, importing the sections it includes
    * The contents of each section would be in their own `tex` files
        > These would be the files that we would actually import into root main. As if you were writing the code directly into the main file, so no `usepackage` or `\begin{document}` or anything like that.
        >
        >This is the reason why it's handy to have a `preview` file to import these sub files to see what you're doing, since they cannot generate a `pdf` without all the usual things that come before the actual document content.

        


## Example
```
images/
    ru-logo.eps (vector image)
khr1/ 
    glosur_khr1.tex
    khr_1-1.tex
    khr_1-2.tex
    khr_1-3.tex
    ...and so on for all sub-sections of the chapter 1
khr2/ 
khr3/ 
glosur_str1.tex (the main file)
```
then the `glosur_str1.tex` contained the following code (excluding the usual `\usepackage` section, for simplisity)
```latex
%------------------------------------------------------------------
% TITLE
%------------------------------------------------------------------
\title{
\centerline{\includegraphics[width=50mm]{images/ru-logo.eps}}
\vspace{0.5 cm}
Glósur Strjál stærðfræði 1
\large  \\
T-117-STR1, Strjál stærðfræði 1, 2019-1 \\ 
\small Reykjavik University - School of Computer Science, Menntavegi 1, IS-101 Reykjavík, Iceland 
  }

\author{
    Stefanía Reynisdóttir\\
    \texttt{stefania19@ru.is}
}

\date{\today}

\renewcommand*\contentsname{Yfirlit} %changes the title of TOC
%------------------------------------------------------------------
% DOCUMENT START HERE
%------------------------------------------------------------------
\begin{document}
\maketitle %Renders title page
\tableofcontents %generates the TOC
\newpage
%KHR 1.1 : Chapter 1: Yrðingarrökfræði =============================
\import{khr1/}{khr_1-1.tex}
%KHR 1.3 : 'propositional equivalances'
\import{khr1/}{khr_1-3.tex}
\newpage
\import{khr2/}{khr_2-1.tex}
\end{document}
```
* It is good to include comments to remind you what you are actually importing
    >It can get quite confusing when it starts to include alot of sections
* The `\newpage` is just for keeping things neat, so sections aren't sharing pages. It's all up to you how you wish to format.
* `\maketitle` will render a title page based on what you specified
* The `\tableofcontents` is essental and wonderful, it will generate a table of contents of everything you have imported automatically. And it links you to those chapters/sections as well, to jump straight to it in the generated `pdf`