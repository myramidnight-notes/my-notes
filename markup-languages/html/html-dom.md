# The Document Object Model (DOM)
The __DOM__ is created when a webpage is loaded, and is mainly used for representing and manipulating the document content, with `JavaScript` and `CSS`for example.

Once the HTML document has been loaded, then the website simply lives as this DOM structure in the browser while being viewed. Any manipulation done to it in the browser only effects the DOM, not the actual HTML document. 
> Good example of DOM manipulation would be when _JavaScript_ is used to generate DOM elements or replace their contents, that become visible on the screen but do not actually exist within the HTML file.

## DOM structure
The DOM is stored as a _tree structure_ based on how things are nested within the HTML document, so there is a clear hierarchy. This can be observed with the use of `CSS` (cascading style sheet) that might target a parent element and have it's style apply itself to all the children as well.

![DOM structure](images/dom-structure-example.png)

>### Example of DOM structure in HTML5
>![HTML5 structure](images/html5-structure.png)
>* This would all be wrapped in the `body` within `html` tag of course.

### DOM nodes
>* Document node
>   * This is the root of the tree, which contains the HTML document. It is a variable that exists within the browser that we can reference to access the document contents. Used with _JavaScript_ for example.
>* Element nodes
>   * The HTML tags
>* Attribute nodes
>   * Attributes applied to the HTML
>* Text nodes
>   * Just simple text that exists within the element nodes.
>* Comment nodes
>   * Comments in the HTML code

#### Node types
* `nodeType` is a property of the DOM that tells us what kind of node we are looking at. 
![Node types](images/dom-node-types.png)

## Querying the DOM
We can target elements within the document within the browser, as the `document` variable lives within the scope of the browser.
```js
const myArticle = document.getElementById('my-article');
```

### Traversing the DOM
>From each node, we can access their `parentNode`, `child Nodes`, `nextSibling` and `previousSibling` along with other properties.
>* `children` is a similar property, which will exclude all text and comment nodes and only contain the element nodes. Then it uses `nextElementSibling`, `previousElementSibling`, `firstElementChild` and `lastElementChild` instead of the previously  mentioned node selectors to traverse through the nodes.

### Manipulating the DOM
> We can edit, create or remove attributes or nodes through the DOM

* `dataset`
* `innerHTML`  
    > Renders a string as HTML
* `textContent` 
    > Render a string as is, HTML tags are not rendered as HTML

### Manipulating nodes
These methods are used on the parent to manipulate their children.
* `appendChild(newElement: node)`
* `insertBefore(newElement: node, nextSibling: node)`
* `removeChild(node)`
* `replaceNode(newNode, oldNode)`


## Dom Elements / Properties
* `id`
    >Unique ID assigned to the element to make it identifiable
* `name`
    >Often used by forms to name the different fields
* `tag`
    > The element tag type
* `class`
    > Classes assigned to an element. For selecting multiple elements sharing a class

### Selecting DOM elements
To select a element by various properties
* `getElementByName`
* `getElementById`
* `getElementByClassName`
* `querySelector` / `querySelectorAll`
    > Lets you use the CSS selectors to pinpoint the element

![Dom node types](images/dom-node-types.png)