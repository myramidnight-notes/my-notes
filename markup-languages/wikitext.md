# Wiki Markup: Wikitext
You can actually use `HTML` elements to formatting a wiki. But the `wikitext` syntax is of course less bulky for the purpose.
## Basic formatting
> Just look at the [Wikipedia page](https://en.wikipedia.org/wiki/Help:Wikitext) for the basic formatting.

### Lists:
* `*` for unordered list items
* `#` for ordered list items
* `;` for term in description list and `:` for all the definitions under it.

### Preformatted text
> Since `HTML` works, you can use the `<pre>` tags.

### Omit formatting the wikitext syntax
* `<nowiki>` will not convert the wikitext syntax into formatting when within these tags.

## Special context
### Table of Contents (ToC)
This is a quick link list to all the headings within a page, which automatically appears above the first heading once a certain number of headings have been included.
* `__FORCETOC__` will force the list to appear, regardless of number of headings.
* `__TOC__` will cause the ToC to appear in a specified location instead of default place.
* `__NOTOC__` will remove table of contents from the page. 

## Templates
You can view templates as a __insert__ feature, which will take everything from within the template page and include it where ever the __template tag__ was used. It is best used for anything that could be reused.

These templates can even take parameters/arguments to create a tailor-fitted instance of the included content.

### Example uses
* Standardized info tables (specially for larger tables, the syntax can take alot of space)
* Specially designed containers for content (such as a banners)
* Clean the page content of long and repetative text.


## Transclusion
* `<noinclude>` is very useful in templates and categories, for anything within these tags will only display on that specific page, but not when the template/category is being used elsewhere. 
    > Useful when adding categories to a template, but you don't want all pages that use the template to include the same tags.
* `<includeonly>` is oposite of the `noinclude`.
    > very useful for templates that will include a category, but the template itself should not be added to that category.
* `<onlyinclude>` will specify exactly what should be included when a template used, but all text within these tags wil be seen on the template page, unlike the `includeonly`.
    > The `onlyincludes` takes priority over the `nowiki` tag.

## Subpages
You can create __subpages__ by using `/` in the page name. But be aware that not all namespaces are allowed to have subpages. Mainly used in `Talk:` and `Help:` or even `User:` namespaces. 
> Let's have a page named `information`, then if you create a page named `information/dk` which actually becomes a page named `dk` which lives under `information`.


Here is a useful template snippet to create a link to a page that lists all the subpages of current page. 
```
[[Special:Prefixindex/{{#if:{{{page|}}}|{{{page}}}|{{FULLPAGENAME}}}}/|{{{1|all subpages of this page}}}]]
```

Good example of usage of subpages would be with a `documentation` template that would automatically include all content of a `/doc` subpage of the specified template page, without having to add anything more than just a `{{Documentation}}` template tag. 