[back to index](../README.md)
* [CSS notes](./sass.md)
# Styling with SASS / SCSS in this project
This guide is specially made for this project, we have our own mixins and rules that might not apply to other projects. 

To read more about the SASS basics, go to the [official SASS guide](https://sass-lang.com/guide)

## What can we do in SASS?
>>>
### $Variables
Just like variables in JavaScripts, these variables can contain any value.
Think of them as design tokens, you should use them for values that can be used in many places,
and still be easily adjusted and changed without having to dig through every instance.
```scss
$primary-color: orange;
$big-font: 30px;
$button-shadow: 1px 0px 8px rgba(0, 0, 0, 0.16);

.myButton {
  background: $primary-color;
  font-size: $big-font;
  box-shadow: $button-shadow;
}
```

### %Extends
What are extends? If multiple elements will share alot of the same style, 
then you can use these extentions to avoid repetition and keep the code tidy.
```scss
//This piece of code will not be printed unless called with @extend
//great for repetative style with slight variations
%shared-style {
  border: 1px solid #ccc;
  padding: 10px;
}

//and this is how you use it
.element-green {
  @extend %shared-style;
  border-color: green
}
.element-red {
  @extend %shared-style;
  border-color: green
}
```

### @Mixins
The mixins can be treated like a style function, because it can be tedious to write some styles with slight variations, not something that @extend can handle. Mixins are perfect for keyframes in CSS animations among other things.

You can even add your extra code into the mixin when using it with `@content`, which will be replaced with the code you provide
```scss
@mixin roundedButton($btn-width, $btn-height){
  padding: 20px;
  font-size: 20px;
  width: $btn-width;
  height: $btn-height;
  border-radius: 8px;
  text-decoration:none;
  @content;
}

//example in use
button.action {
  @include roundedButton( 300px, 10px ) {
    border: solid 1px green;
  }
}
```

### Math operators
Yes, you can let SASS do math for you. Want to calculate the percentage based on pixels?
```scss
.element {
  width: 600px / 960px * 100%; //width: 62.5%
}
```
### Lists / Loops
This is interesting, you can create a loop that renders a style
```scss
$colors: red yellow blue;

@each $color in $colors {
  .swatch--#{$color} {
    background: $color;
  }
}
/* The output CSS would be:
.swatch--red {
  background: red;
}

.swatch--yellow {
  background: yellow;
}

.swatch--blue {
  background: blue;
}
*/
```

### Functions()
We can create functions in SASS, they can be simple, or they can be [very complex](https://github.com/pierreburel/sass-rem/blob/master/_rem.scss).
```scss
@function px-to-rem($px) { 
	@return ($px / $base-font-size) * 1rem;
}

h3 {
  font-size: px-to-rem(28px); //font-size: 1.75rem;
}
```

### Playing with colors in SASS
Since we're using SASS for the styling, then we can actually use whatever coloring system. SASS will let you use HEX color values and convert them to RGB or RGBA.

* The HEX color `#0000FF` for blue can be converted to RGB with `rgb(#0000FF)`
* If you want transparency by using RGBA alpha channels, then `rgba(#0000FF, 0.5)` does the trick.
* You can darken/lighten color with `darken(#0000FF, 10%)` or `lighten(#0000FF, 10%)`
* They have `tint`, `shade`, `saturate`, `desaturate`, `grayscale`, `invert` and [many more fun color functions](https://sass-lang.com/documentation/functions/color)
>>>

## Our SCSS toolbox, project specific
>>>
### Breakpoints mixin
This is just some notes about the usage of our `@mixin` that handles the @media breakpoints.
The actual pixel values of each breakpoint can be found and adjusted in the `_breakpoints.scss` partial.

* Best practice would be to create a good base style aimed at mobile, it is easier to expand rather than trim down. That means that the breakpoint mixin should only be used for style that deviates from the basic style (such as styles for larger screens)

```scss
@include breakpoint(xsmall){
  //any style here will only take effect on mobile/small screens, set to a max-width.
  //perfect for adding "display:none" to hide things that shouldn't appear in mobile
}
@include breakpoint(small){
  //Style that takes effect at the min-width of small
  //there is also 'medium' and 'large'
}
```
>>>



