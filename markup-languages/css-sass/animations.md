# CSS: Animations
You can actually do animations with `CSS3`, anything from changing size of a button when hovering over it, to complicated animated scenes worthy of flash-animations.

Subtle animations are quite essentials

### Ways to use CSS animations
* add visual feedback to events, such as hover or click
* transitions between screens 
* animate a image (SVG)
* Create animated scenes with timed animations

## Animation Keyframes
Animations will effect a targeted DOM element, applying changes to colors, sizes, positions and rotations. We can indicate what style should apply at a given point during the animation sequence.

Telling an element to be `100px` at the start, and `20px` at the end, will result in a smooth animation showing the element changing between given dimensions, instead of happening instantly. Any style can be provided, but it will only apply to the element.

```css
@keyframes animation-name {
    /* details of the animation */
}
```

We can use `from` and `to`, telling the style what the start and end results would be for the given animation.

Can also specify percentages to indcate when during the transition something should change. `0%` would be the start frame `50%` would indicate when the animation is halfway through, `100%` would be the endframe.

### Applying the animation
```css
div {
    animation: animation-name 5s ease-in infinite;
}
```
> The given example would apply the specified animation to the `div` element, `5s` is the duration that the animation will take to complete, and it will loop infinitely. 

#### Animation properties
1. animation name: __name of `@keyframes`__
    > A defined animation keyframe can have any given name
1. duration of animation: __`0-N`s __
    > How long should the animation sequence be?
1. timing function: __linear/ease/step-end/cubic-bezier__
    > How does the animation start, does it fade into view? 
1. delay before playing animation: __`0-N`s__
    > Perhaps you don't want it to play instantly when the site loads.
1. iterations of animation: __number/infinite__
    > How often should the animation loop? once, few times, infinitly?
1. direction: __normal/reverse/alternate__
    > Does it start at the end, or at the beginning?
1. fill mode: __forwards/backwards/both__
    > The end state of animation, should it stay at the end frame, or return to the start?
1. play state: __paused/running__