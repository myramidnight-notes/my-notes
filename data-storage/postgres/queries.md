## Getting all column names ad data types with query
```sql
SELECT column_name,data_type 
  FROM information_schema.columns
 WHERE table_schema = 'public'
   AND table_name   = 'Users';
```