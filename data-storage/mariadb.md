# MariaDB
MariaDB is a forked version of MySQL that was developed because of concerns after _Oracle_ acquired MySQL. 

* Includes a wide selection of storage engines
    * Has `NoSQL` support
* Uses standard and popular querying languages
* Supports `PHP`
* Offers many operations and commands unavailable in MySQL and eliminates/replaces features that impact performance negatively.

> [MariaDB vs MySQL](https://www.opensourceforu.com/2018/04/why-mariadb-scores-over-mysql/)

## Install
> [Installing MariaDB on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04)

After using `apt instal mariadb-server`, use the included `mysql_secure_installation` to set it up.

### Opening database in terminal
the following command will open `mysql` (which is actually mariadb) with a specified user and prompts for password
```
mysql -u username -p
```

If you can open the database simply by using `mysql` without prompting for password, which is the default behaviour, then you might want to adjust the user permissions.

### Changing password of user in database
```sql
ALTER USER 'username'@'localhost' IDENTIFIED BY 'newPassWord';
FLUSH PRIVILEGES;
```