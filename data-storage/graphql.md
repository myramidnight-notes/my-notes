# GraphQL 
> I was using `NodeJS`, but [GraphQL has great language support](https://graphql.org/code/).   

It is a __query language__ for fetching requested information. It allows the requester to customize exactly how the data should be returned, down to the order and number of fields.

### Still evolving
GraphQL is relatively new and therefor constantly changing, which could be considered a negative point. But it has so many nice things going for it that it's might be worth having to figure things out all over again as it evolves.

### Automatic documentation
You can easily generate documentation based on the code, which will list what is expected in the request and what can be returned. If you are using a tools which generates a `gui` for a GraphQL server

## Apollo server for GraphQL
This server has useful tools included such as `graphql-playground-html` that is automatically deployed when the server is told it's running GraphQL for development to test out the requests and see what the response looks like.
> The `package.json` should include the following to run GraphQL on Apollo server.
```json
{
    "scripts" : {
        "start": "nodemon -e js,json,graphql",
    }, 
    "devDependencies": {
        "nodemon": "^1.18.4"
    },
    "dependencies": {    
        "apollo-error-converter": "^1.1.1",
        "apollo-server": "^2.18.1",
        "graphql": "^15.3.0",
    }
}
```

## Schemas and Resolvers
A GraphQL code is basically split into two halves, where you have schemas that regulate the shape of the data on one hand, and the resolvers on the other hand which is where you program how the data is processed from the request. 

Resolvers would then interact with the resorces you have available. These resources could be in different databases or even recieved from other APIs. This allows the user to just send a single request and get that they requested instead of manually sending multiple requests to 

### We do NOT return null
You have to be careful about any possible null values and catch them like errors. You can specify what to return in these cases or throw errors/exceptions that would be processed through error handlers if you have them (you should have them).
these resources to get what they wanted.

### Possible reusage of schemas.
It might be useful to research the tools made available with whatever database resource you plan to use. For example there are tools that let you generate GraphQL schemas from `mongoose` if you're using __mongodb__. 

This is the only example I can give, but it is always worth doing some reasearch related to the resource you plan on using, might never know if someone made a tool to make your life easier.

## Requests and Responses
When you write a request to graphQL that is expected to return a object, then you are required to specify what fields you want returned, even for the subfields if those are objects. It gives you errors if you fail to specify those fields.

You can have the data tailored to your needs with the ability to rename the keys with aliases in the response.

### Queries and Mutations
There are these two types of requests, __queries__ for simply reading the data, and __mutations__ that let you change the data (create, update and delete). 