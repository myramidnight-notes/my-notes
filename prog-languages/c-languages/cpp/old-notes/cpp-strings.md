```
#include <iostream>
#include <sstream>
using namespace std;
```

### Stringstream
```cpp
string myString(int name){
    //create the string stream
    stringstream ss;
    //compose your string stream, similar to cout
    ss << "How are you doing, " << name << "?";
    //turn the stream into a string
    return ss.str();
}
```