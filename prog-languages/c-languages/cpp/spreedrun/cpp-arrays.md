# Arrays in C++
> It is important to understand pointers to really figure out how arrays work in C++
* Arrays are essentially groups of multiple data (of the same type)
    >And since memory is linear, the data will be arranged in order, a list.

```cpp
int example[3];  //Array named example, contains 3 integers
```
* The example above will allocate memory for 3 integers in a row
    >But since it does not initiate their values, each of those memory-slots will just contain some _junk_ data (it all depends on what was using that memory previously).
* You access individual values by specifying their index
    ```cpp
    example[2]; //will simply return the 3rd item in the array
    example;    //will return a pointer to the first item in the array
    ```
* Trying to write something to an item outside of the range of the array will cause errors
    >Memory Access violation, trying to access something that isn't yours. And you will not know where or what happened (unless you're debugging).
    >
    >The issue is that you might be messing with data that has been allocated to something else.
* Memory allocated to arrays is in a row 
    > all items are adjacent to each other. So indexing is just starting at first item in the list, then offset by the index times the size of individual item type.
    >
    >So indexing arrays is fast!
    ```cpp
    int example[5];
    int* ptr = example; //arrays are just pointers to first element
    for (int i = 0; i < 5; i++){
        example[i] = 2;
    }

    example[2] = 5;
    *(ptr + 2) = 6; //does the same thing as the line above
    *(int*)((char*)ptr + 8) = 7; //same as the line above
    ```
    >Adding + 2 to the pointer will depend on the type being stored in the array.
    * The above example just gets created on the stack, so `example` will not live outside the current scope.

    ```cpp
    //same as 'example', just allocated on the heap this time
    int* another = new int[5]; 

    delete another[]; //how we delete arrays on heap
    ```
    * This example, the `another` array will outlive the scope, meaning it will still exist when you exit the scope. 
        >This also means that you need to delete it specially to prevent memory leaks.
* We cannot just _get_ the size of an array in C++ (number of items)
    >we simply have to know how many items it will hold (since we specify it, so the size would be the number of items multiplied by the size of the item type.
    ```cpp
    //This only works for arrays stored on the stack
    int a[5];
    sizeof(a) / sizeof(int); //this will give us number of items in the array
    ```
    * The `sizeof` an array stored on the heap is just the size of the pointer.
    * Best we can do is to create a class that handles your arrays that could store the information you generally want (such as total number of items it contains).
    * There is an `std::array` in C++11 which will include size, bounds checking and other neat things (that we might take for granted in other higher level languages)

## Strings
Strings are just arrays of `char`
```cpp
char  myString[7] = {'Y','o','u','r','N','a','m','e',0};
char* myString2 = "YourName"; //Does the same thing as above
```
* Strings are always defined as the length of the string in characters + 1
    >This is for the null-terminator, `'\0'` or `0`, which indicates the end of the string.
* In the case of strings, `char*` does not actually mean that it is stored on the _heap_. As the example shows, those two lines do the exact same thing and allocate memory on the stack.
* Strings are immutable, just like the array that it is
    >Because you can't suddenly make the string longer, that would require reallocating memory for a larger array.
* You can modify a string to contain shorter strings, but not longer
    >If the null-terminator is missing in the string, then it will continue reading until it finds a `'\0'`  somewhere along the way (so it is reading outside of it's allocated memory).
* And there exists the `std::string` which allows you to have dynamic strings (that can change size and such) in a simple way.
    >It does a lot of things for you and makes strings easier to handle, by containing a lot of handy methods for manipulating the string.

### String Literals
>It is just text between two double quotes `"My String literal"`