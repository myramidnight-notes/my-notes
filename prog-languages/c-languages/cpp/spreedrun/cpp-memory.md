# Memory in C++
Everything works from memory, so it is very important. Programs are loaded into memory when they are running. When we talk about memory, we are basically just talking about the RAM (random access memory), which is actually divided into two sections. 

They are generally the same, just how they allocate memory is different. 

So the memory is split into two sections:
1. The Stack
    >Predefined size of memory reserved for running programs. We do not manage the stack. This is where everything lives that only exists within a scope.
    >
    >* Everything is stored in order. 
    >* We essentially just use the stack to store memory for __scopes__.
    >* The stack will keep track where it was whenever we are entering new scopes, to quickly know where to return to when we leave a scope.
    >* There is a _stack pointer_ that points at the _top_ of the stack, and whenever something is added to the stack, the pointer will move to be pointing at the top again. It returns the current memory address that it happens to be pointing at.
    >* This makes the stack __very fast__, because we do not need to find free space in memory, we just have to find  where the _stack pointer_ is and put the data there
    >* If you can actually look at the memory, then keep in mind that they are stored as _little endian_
    >* So the stack doesn't bother with _freeing_ memory, the pointer just moves to where it needs to be, and everything beyond it is considered free.
1. The Heap
    >The heap is more dynamic memory, where we can keep things that should exist outside of any scope.
    >* We use the `new` keyword to allocate anything to the heap
    >* Every value we store on the heap essentially gets stored in random places on the heap (wherever it finds free space, depends on the algorithm how it finds the space)
    >* The variable will not actually hold a value, but a pointer to the appropriate spot in memory.

```cpp
int main(){
    //Stack examples-------------------
    int value = 5;  
    int array[5] = {}; //setting all values to zero
    MyClass classInstance;

    //Heap examples -------------------
    int* hValue = new int; 
    *hValue = 5; //Setting the value through de-reference

    int* hArray = new int[5];
    MyClass* hClassInstance = new MyClass();

}
```