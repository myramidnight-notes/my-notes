# Classes in C++

*  A _member function_ is a function that is a belongs to a class (a _method_).
* The `this` keyword is only accessable within _member functions_
    >It is reference to the current instance of object
    ```cpp
    //What 'this' actually is (using 'Entity' class for example)
    Entity* e = this; //Is 'Entity *const this'
    ```
* So `this` is a pointer

#### Class vs Struct
There is basically no difference between using a _class_ and using a _structure_, the only difference would be __visibility__ of the properties and functions within.
* A __struct__ will have everything __public__ by default if you specify nothing
    ```cpp
    struct myStruct
    {
        int myProp; //this will be public

        private: //everything below this will be private
        int myProp2;
    }
    ```
    >Generally we tend to use `struct` to define data objects that we would want to pass around, to keep information organized. Something that contains only variables, no functionality.
    >
    >It's main reason it exists within _C++_ is to maintain backwards compatability with _C_ (which has structures, but not classes).

* A __class__ will have everything __private__ by default if you specify nothing.
    ```cpp
    struct myClass
    {
        int myProp; //this will be private

        public: //everything below this will be public
        int myProp2; 
    }
    ```
    >Classes are great for organizie your code by purpose, they can be small or large. Good basic class to define that will be very useful to create is a _logger_ that can filter out what prints out to console.
    >* [How to Write a C++ class](https://www.youtube.com/watch?v=3dHBFBw13E0&list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb&index=20)

### Visibility?
What is visibility? It means how accessable is the property from outside of the class. 
* __Public__ will allow anything to interact with the property
* __Private__ means the property is only accessable from within the class
* __Protected__ 

* You can specify if something should be `private` or `public`, and you can use them multiple times
    >It is useful for organizing however you want. Perhaps you want to group variables together and functions together, but each group would split up into public and private. 


## Basic Constructors (initiate objects)
>A constructor is basically a special type of method which runs whenever we create a new instance of an object.
* It is the _init_ of the class
* When defining it, it is just the name of the class as a function
    ```cpp
    class Entity {
        //The constructor
        Entity(){ 
            //Code that runs when we create a new instance
        }
    };
    ```
    * We can optionally define parameters
        >like when defining any other function/method

* We can also define as many constructors as we want, provided that each have different set of parameters
    >Different ways to initiate the class
* We can actually declare functions within the class, and then define them outside
    ```cpp
    class Entity {
        //The constructor
        Entity();

    };
    
    Entity::Entity(){ 
        //Code that runs when we create a new instance
    }

    ```
    >This lets us keep the class itself compact as a decleration, and keep the definitions elsewhere.

### Controlling how we create inscances 
* We can restrict how classes are initiated, by making the default constructor (no parameters) a __private__ member of the class. 
    >Then we are forced to the public constructurs that have been defined
* Giving the constructor the value `delete` will also prevent instantiation of the class
    ```cpp
    class Entity{
        Entity() = delete;
    }
    ```

### How to initialize variables through constructor
```cpp
class Entity{
    int x,y;
    Entity(int x, int y) //using an 'initializor list'
        : x(x), y(y){}
};
```
```cpp
class Entity{
    int x,y;
    Entity(int x, int y) {//The 'this' keyword
        this->x = x;
        this->y = y;
    }
};
```

## Desctructor (destroying objects)
>It is the oposite of a constructor, a method that runs when a instance is destroyed
* A __Destructor__ is defined to make sure everything that should be destroyed is destroyed
    > Anything that the object might have stored on the heap manually needs to be deleted as well, to prevent memory leaks.
```cpp
class Entity{
    Entity(){};     //Constructor
    ~Entity(){};    //Destructor   
}
```

## Sizeof class
* So apparently methods are not counted as part of the size of a class
    >Only the combined the size of the defined variables is counted. Unless we are redefining functions (when inherititing functionality)
    >
    >Just use `sizeof(type)` to confirm byte sizes