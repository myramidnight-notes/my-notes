# Inheritance in C++
The ability to create sub-classes from other classes, generating a hierarchy of classes that are related.

* The main advantage of using class inheritance is to avoid __code duplication__
    >Everything it inherits does not need to be redefined. Then we only need to define what is different from the parent class.

```cpp
class Entity {}; //Our base class

class Child: public Entity {}; //Our child class
```
* The `public` keyword is telling us that we want to inherit everything that is _public_ in `Entity`. 
    >Then we can use anything publicly defined in the parent, within the Child class without having to define it.
* The child will inherit all the ancestrial classes
    >So our `Child` class is of type `Child`, but it is also type `Entity`. This allows us to declare that something returns type `Entity`, then any class that has `Entity` as an ancestor will qualify.
* A sub-class only has access to anything defined as `public` or `protected` in the parent. 
    >Essentially anything that is _public_ from any ancestor is accessable.
* A sub-class can redefine functionality it inherits

## Overwriting functions
>[Virtual Functions (video)](https://www.youtube.com/watch?v=oIV2KchSyGQ&list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb&index=27)


### Without using Virtual
```cpp
class Animal{
    public:
    std::string GetName() { return "Animal";}
};

class Mammal: public Animal{
    std::string GetName() { return "Mammal";}
};
```
* The example above allows us to create instances of Animal and Mammal, and call the `GetName` function on both of them to print out the corresponding name.
* The example will break when we try to use __polymorphism__
    >Since `Mammal` will have inherited from `Animal`, it will also be of type `Animal` and should technically be handled as if it were simply an instance of `Animal`. 
    ```cpp
    Animal* myAnimal = new Animal();
    Mammal* myMammal = new Mammal();

    //Function that prints any Entity type
    void Print(Animal* animal){
        std::cout << animal->GetName() << endl;
    }

    Print(myAnimal); //Prints 'Animal'
    Print(myMammal); //Prints 'Animal'

    ``` 
    >We might have expected the _Print_ function to have printed "Mammal" when we passed it our `myMammal` instance, but since the function defines the argument as Animal (not a Mammal), so that's where the _print_ will look for the definition of `GetName`


### With Virtual
* We need to define a function as `Virtual` in the base class, which just says "This method may be overridden by a sub-class"
* The `virtual` keyword introduces __dynamic dispatch__
    >It is something the compiler typically implements with V tables, which is a table that contains a mapping of all our virtual functions from our base class, so that we can map them to the correct overridden definition in runtime.

So for our previous example, if we would define `GetName` in `Animal` as a virtual function, then it would print as we had expected when we passed the `Mammal` instance to the _print_ function.
```cpp
class Animal{
    public:
    virtual std::string GetName() { return "Animal";}
};

class Mammal: public Animal{
    std::string GetName() override { return "Mammal";}
};
```
* Adding the `override` is optional, but it makes things clear what is going on in the code. It also makes sure that we are targeting a virtual function (so if we wrote the name of the virtual function incorrectly, we would get an error).

>Using _virtual_ makes our code more expensive, because we will need to store that __V-table__ for the compiler, and a extra pointer that points to the V-table. Finally we will need to go through that V-table every time we call a virtual function.
>
>This only really matters if you are having terrible performance.

### Pure Virtual Functions (Interface)
> After listening to Cherno on the topic, the first thing I thought of as comparable was from _python_ when using `ABC` (abstract classes) and `@abstractMethod` that would complain if the child class didn't create it's own definition of the specified method.

A class that only contains declarations without definitions would be an __Interface__ (it's not an actual keyword in C++, it is just a class), and instances of it cannot be made because the definitions are missing.

* It allows us to only declare methods in the base-class, and then force the sub-class to create the definition.

Previous example as pure virtual function
```cpp
class Animal{
    public:
    virtual std::string GetName() = 0; //this forces the sub-class to implement it.
};
```
