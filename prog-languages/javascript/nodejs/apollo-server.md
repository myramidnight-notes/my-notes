# Apollo server
This seems to be a very good alternative to `express` server when doing a GraphQL API. It includes many useful tools, such as the `graphql-playground-html` that is automatically deployed when the server is told it's running GraphQL for development to test out the requests and see what the response looks like.

## Addons for Apollo server
### Apollo error converter
Found [error handler](https://www.npmjs.com/package/apollo-error-converter) because all the thrown errors were not returning responces, just crashing the server. And it works. Don't need all the try/catch just to handle the thrown errors with limited results.