# NodeJS
NodeJS is basically just javascript, which is a programming language executed in the client to give websites functionality. 

`NodeJS` just takes the javascript engine from the client (google chrome specifically) and lets you run it server side, like any other programming language, such as `Python` or `C#`

## NPM (node package manager)
This is how you install or uninstall dependencies for your project. Whenever you install something, it will be added to the `package.json` under dependencies. This allowes `npm` to easily install all the dependencies with a simple command `npm install`.

### Custom `npm` commands
You can store CLI commands within the __scripts__ section in the `package.json`. 

You specify the name of the command, such as `start` or `test` and basically just store the CLI command under it so you do not need to manually write it every time.

Then you just have to run it by name, `npm start` or whatever you named your script.

### Node modules
There are so many tools and packages available for your project, and your `node_modueles` folder is where they are stored. This folder gets big pretty fast and is normally hidden from repositories with `.gitignore` for good reason. 

#### Do not include the modules folder

Best practice is to never include the `node_modules` folder anywhere, be it the online repository or zipping/storing the project. It is extra baggage that nobody needs, specially if they just want to view the code. 

## NPM packages 
There are just too many tools and frameworks to choose from, some are small (such as `moment`) for a single purpose and some are giant frameworks. You can create your own modules quite easily, but there is always a big chance that someone has already made what you're looking for. Why reinvent the wheel unless you're learning how it rolls?

The ones mentioned below stood out as I used them for school projects, creating API and other applications. 

* React framework
    > Easily generate a frontend for your application
* Express server
    > Popular and well known server for your API
* [GraphQL](./nodejs/graphql.md) with Apollo server
    > Query language for your API that even generates it's own documentation
* [Mongoose](./nodejs/mongoose.md) for mongoDB
    > Manages your mongoDB connection and had schemas to regulate the data