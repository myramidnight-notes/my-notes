# Typescript
* Javascript with syntax for types
* Typescript compiles into Javascript
* some features are similar to Java

We all know about javascript, it's the programming language to make things happen within internet browsers

Typescript seems to be taking over Javascript in popularity among programmers, due to the fact it adds _type safety_ to javascript, which makes the code cleaner and less errors.

You can even specify what version of Javascrip the code will compile as, allowing you to test what works for older versions.


### Installing Typescript
> [Guide on typescript](https://www.typescriptlang.org/download/)

```
npm install typescript
```