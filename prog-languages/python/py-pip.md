# Package Manager for Python (PIP)

## Install requirements from file
>The text file would contain something like is printed out with `pip freeze`, listing all the core packages required for the project. 
>```
>pip install -r requirements.txt
>```

## See list of installed packages
The following command will print out the list and versions of all installed packages that the project has access to. When in a virtual environment, it will just print out the packages contained in the _venv_.
>```
>pip freeze
>```