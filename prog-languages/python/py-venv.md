Why would you want to use a virtual enviornment when you can just install things on your computer and run the code?
* Virtual enviornment lets you run things in a controlled environment, specially useful when working on a project with otherse and you can't control what your teammembers have installed globally.
* It can help you avoid conflicts, where some globally installed things could mess with packets you want to use. 
* A virtual enviornment isn't effected by what's installed globally.



## Creating the VENV
You might need to install `virtualenv` first, if you have issues creating the `venv` (then just run `pip install virtualenv`)
1. Create the virtual enviornment:
    ```
    py -m venv venv
    ```
    * `py -m venv <dir-name>`
1. Activate the enviornment
    ```
     .\venv\Scripts\activate
    ```
    > There might be issues if your system does not allow scripts to run.You should read and perhaps google the error message that comes:
    >* `<script>` cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
    >
    >On windows the solution seems to be to run: 
    >* `Set-ExecutionPolicy Unrestricted -Scope Process`
    >   > it will allow running virtualenv in the current powershell session (we might not want to completly remove all restrictions, it might mess your computer up, it is a security feature after all)
1. Now all commands and code run in that terminal window will be run within a virtual enviornment (you will notice `(venv)` at the start of the line, indicating that it's active. Might even be green if your terminal has colors )
    * The `venv` is only active in the terminal you activate it in, so it's not a global activation. 
1. With the `venv` active, you can install what you need for the project
    > It is very handy to have a requirments file that allows you to just install everything you need with one command, specially when working in a team:
    >
    > `pip install -r requirements.txt` 
