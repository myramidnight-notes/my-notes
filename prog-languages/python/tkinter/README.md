
* [GUI programming with Tkinter (Real Python.com)](https://realpython.com/python-gui-tkinter/)
* [Guide to tkinter frame](https://www.pythontutorial.net/tkinter/tkinter-frame/)
* [Create GUI with Python and Tkinter (youtube playlist)](https://www.youtube.com/watch?v=yQSEXcf6s2I&list=PLCC34OHNcOtoC6GglhF3ncJ5rLwQrLGnV)

# Using Tkinter to create GUI
> The really nice thing about Tkinter is that comes as part of the Python3 standard library, so you don't have to install anything specially to use it.

## Using Tkinter
1. Import `tkinter`
    ```py
    import tkinter as tk
    ```
1. First thing we need is to create the GUI window: 
    ```py
    window = tk.Tk()
    ```
1. Define widgets
    >EVERYTHING is a widget, the lables, buttons, frame, input... just pick one.
    ```py
    label = tk.Label(
            text="My text label!",
            foreground="green", #the text color
            background = "purple", #text highlight/background
            )
    ```
1. Add the widget to the screen
    >Nothing gets added automatically, you can use `pack()` or `grid()`
    ```
    label.pack()
    ```
1. Start up your program loop
    ```py
    window.mainloop()
    ```
1. Run your program!

### Using frames
> I would find it important to my organizing to be able to group things together visibly, and we can use `LabelFrame` to do that, it will let you frame widgets and give that section a label.
1. Define your LabelFrame
    ```py
    frame = tk.LabelFrame(
            self.window,
            text="User Input",
            font=(20)
        )
    ``` 
1. Set the frame as parent of the widges you want in it
    ```py
    button = tk.Label(
            frame,
            text="Click Me",
            bg="orange",    # background
            fg="yellow"     # text color
            )
    ```
1. Add your frame and widgets to the window
    ```py
    frame.pack()
    button.pack()
    ```
# Making a GUI for our program, using Tkinter
>It seems that this is a GUI framework that is built into the Python standard library, which makes it very accessable and you don't have to install anything specially to get it (just need to have  __Python 3__).

## The Window
This is where all GUI live, within a window. The frame of a window usually depends on the operating system (unless something allows you to customize that), but that is just a frame, not part of the program.

## Links
* [GUI programming with Tkinter (Real Python.com)](https://realpython.com/python-gui-tkinter/)
* [Guide to tkinter frame](https://www.pythontutorial.net/tkinter/tkinter-frame/)
* [Create GUI with Python and Tkinter (youtube playlist)](https://www.youtube.com/watch?v=yQSEXcf6s2I&list=PLCC34OHNcOtoC6GglhF3ncJ5rLwQrLGnV)
