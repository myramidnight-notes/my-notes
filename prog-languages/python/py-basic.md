[back to language index](../README.md)
# Just some notes about python
Because when the course is being taught in Icelandic, it gets confusing to keep up to the [Programming Vocabulary](../program-vocabulary.md)



## Running a file
* `py path-to-program.py`
* `py -m path-to-program.main`
  > This is handy when you get errors about `ModuleNotFoundError: No module named 'src'` even when it's supposed to work, because then you are essentially specifying where the root of the program should be (that would be the location you are calling the program from), at least that is how I see it.


## Namespaces ('nafnsvið')
>`namespace` is the identifier and it's value.

names must begin with a letter or underscore `_` (starting with a underscore has special meaning), __not__ numbers.

### Variables ('breytur')
There are many ways to make variables readable, the only rule is to just pick one method and stick to to it throughout project, __keep the names descriptive of purpose and stay consistant__

Here are the 3 most commonly used methods:
* Camel Case: `exampleOfVariableName`
* Pascale Case: `ExampleOfVariableName`(same as camel case, just starts with a capital letter)
* Snake Case: `example_of_variable_name`

## Operators
The list of precedence of different operators.
1. `()` parenthesis ('svigar')
1. `**`  exponention ('veldisvísar') example: `x**2`  ('x í öðru veldi')
1. `+x`, `-x`  Positive / Negative
1. `*`, `/`, `%`, `//` multiplication, division, remainder, quotient 
1. `+`, `-` addition, substraction
1. `<`, `<=`,  `>`,  `>=`,  `!=` : comparisons 
1. `not x` boolean NOT
1. `and` boolean AND
1. `or` boolean OR

## Conditionals
> `if`, `elif` and `else` allow you to chain conditions, sections of code that will only run if the condition is met. 
```python
x = 3
y = 8
if x < y: #if this statement is true
  print("x is smaller")
elif x > y: #extra 'else' statements.
  print("y is smaller")
else: #if no other condition is met, run this
  print("something else")
```
`else` and `elif` creates conditions where only one of them is run, asking each time if the statement is `true`. 

On the other hand, you can have multiple `if` statements that will all run (if this, and if this...)

## Loops
>`for` loops itterate through a collection, such as a range of numbers, list or string (stings are a collection of letters).
```python
for item in collection :
  print(item)
  
for letter in "Loop Example":
  print(letter) #would print one letter per line.
```

> `while` loops itterate as long as the statement is `true`, connected to a variable outside of itself that can become `false` in order to stop the loop. 

```python
counter = 0
while counter < 10 :
  #increase the counter by 1 with each itteration
  counter += 1 
  #Print a line whenever it loops through
  print('This while loop has run ' + str(counter) + " times")
else:
  #block of code that runs once the loop has ended.
  print('the loop has ended')
```
* `else` statement is optional and can be used with both `for` and `while` loops to run some code after the loop has finished itterating. 
* `break` will end the loop instantly, it even skips the `else` part.
* `continue` will tell the loop to go back to the beginning of loop again

## Functions 
You can define a function with `def`, which will contain a block of code that will only be run if the function is called. 

Functions can have 0 or more `parameters`, and you feed these parameters `arguments`. These become variables that are used within the scope of the function and are set when the function is called.


```python
def sum_of_numbers(num1, num2):
  #will return the sum of the two numbers
  return num1 + num2

print(sum_of_numbers(3,5)) # '8'
```
> It is best practice to have a empty line below a function to seperate it from the following code.

### Defining parameters and arguments
#### Default values
You can define a default parameter value that will be used if the user does not provide an argument for some parameters, `var=default_value`
```python
def sum_of_numbers(num1=2, num2=2):
  #will return the sum of the two numbers
  return num1 + num2

print(sum_of_numbers(3,5)) # 8
print(sum_of_numbers()) # 4
```

#### Defined data type
You can define what type of value should be used for argument, as `var:type`, if it should be a `int`, `float`, `string` or any other data types in python.
```python
def sum_of_numbers(num1:int, num2:int):
  #will return the sum of the two numbers
  return num1 + num2

print(sum_of_numbers(3,5)) # 8
print(sum_of_numbers()) # will use default values = 4
print(sum_of_numbers(4)) # will use default for second argument = 6
```

### Docstrings 
You can describe a function within the __docstring_ to tell the user the purpose of the function and how to use it, which can be prompted with `help(function)` or  `function.__docstring__`. To define a _docstring_, just add a triple quotes as the first item in the function scope.
```python
def sum_of_numbers(num1=2, num2=2):
  """will return the sum of the two numbers""" #the docstring
  return num1 + num2

print(sum_of_numbers(3,5)) # 8
print(sum_of_numbers()) # 4
```

## Lists
These are two types of lists in python, while normal lists are mutable, tuples are imutable just like strings. 

Lists can contain integers, floats, bool, strings, lists, tuples. Lists containing other lists gives it more dimention.

```python
[1,2,3] #it can contain single type of values
['string', 0.4, 1, True, ['a','b','c'], False] #or multiple type of values
```
### Tuples (imutable lists)
Tuples gives you a list that cannot be accidentally changed and is there for reliable. Instead of square brackets `[]`, tuples use normal parenthesis `()` where each element is seperated by a comma `,`. It is optional to omit the parenthesis when creating tuples with more than one element. 
```python
(1,2,3) #(1,2,3)
1,2,3   #(1,2,3)
(1,)    #(1,)
(1)     #1        not a tuple
```

>If the tuple only contains a single element, then it is important to still add a comma after the element to indicate that it is indeed a tuple.

### list methods
There exist many methods for use with lists and tuples (though anything that alters the list will not work on tuples). 
```python
my_list = ['a', 'd', 'b', 'c']
my_sorted_list = sorted(my_list)  # ['a', 'b', 'c', 'd']
my_sorted_list = my_list.sort()   # none, .sort() returns nothing
my_list.sort()                    # ['a', 'b', 'c', 'd'] 

```