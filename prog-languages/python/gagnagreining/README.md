# Glósur tengdar gagnagreiningu
### Useful packages for data analysis
* `Pandas`
    > For turning data into dataframes for plotting
* `Matplotlib`
    > Package for plotting various graphs from data
    * `mpl_toolkits.basemap` 
        > Useful toolkit for matplotlib to create maps to plot geographical data.
* `Numpy` 
    > Makes it easier to handle matrices and do math on them.

## Matplotlib
