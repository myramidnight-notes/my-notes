# Plotting maps (basemap)
* [Basemap documentation](https://matplotlib.org/basemap/)
* [Basemap tutorial (readthedocs.io)](https://basemaptutorial.readthedocs.io/en/latest/)

> Basemap is part of `mpl_toolkits.basemap` package, that builds ontop of `matplotlib` to allow you to generate geographical maps as backgrounds for your plotting needs.

### [Base Parameters](https://matplotlib.org/basemap/api/basemap_api.html)
> There are many more parameters, just wanted to make a few notes about a few of them.
* __Resolution__
    > The resolution effects how fast the map is drawn, so pick the apropriate resolution for your mapsize. Resolution drops by 80% between datasets
    >* `c` = crude
    >* `l` = low
    >* `i` = intermediate
    >* `h` = high
    >* `f` = full
* __Plotting mapsize with coordinates__
    > `lat_1`,`lat_2`, `lon_1` and`lon_2` are the two points on the map you want to use.

### Example of a basemap
```python
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt

plt.figure(figsize=(15,15))
iceland = Basemap(
            width=2500000,height=1800000, # size on map in meters
            projection='lcc', # type of map projection
            resolution='l', # quality of the mpa resolution
            lat_0=65, lon_0=-19, # center of the map
            lat_1=-24.25, lon_1 = 66.37, # first point (corner)
            lat_2=-12.05, lon_2=63.37 # second point (opposite cornor)
           )
iceland.drawcoastlines() # draws in the coast lines
iceland.drawlsmask(land_color='lightgray', ocean_color='lightblue') # colors the map
```
![Basemap](../images/matplotlib-basemap.png)

## Plotting onto the map
1. You should probably use `plt.figure()` to contain your figure and plot (it allows you to add titles and what not, but it's optional), by placing it before you render your map.
1. Then you have to collect the geographical locations of your plot points. 
    * `longitude` are collected into an array and the `latitude` in a separate array. 
    * You can collect additional information if useful, such as size/color of each point. The index of each of these arrays will pair up to be applied to the generated plot point.
1. Generate the plot coordinates based on the geographical points we collected.
    * `longitude` becomes the `x array` and `latitude` becomes the `y array`
1. Then apply a `scatter` plot to the map, feeding it the `x array` and `y array`

### Example
> This example is using the same map as we generated above, so there is no need to reestablish the `iceland` basemap. The data comes from a JSON api about earthquakes in Iceland ([apis.is](apis.is))
>```python
>eq_locations = pandas.DataFrame(data=eq_data[['latitude', 'longitude', 'size']])
>
># generating the plot figure
>plt.figure(figsize=(15,15))
>iceland.drawlsmask(land_color = 'lightgray', ocean_color='lightblue')
>iceland.drawcoastlines()
>
>eq_long = []
>eq_lat = []
>eq_size = []
>for index in eq_locations.index:
>    x = eq_locations['longitude'][index]
>    y = eq_locations['latitude'][index]
>    size = eq_locations['size'][index]
>    # collect the values for each plot
>    eq_long.append(x)
>    eq_lat.append(y)
>    eq_size.append(size*100)
>    
>#generate the X and Y for coordinates
>eqX, eqY = iceland(eq_long,eq_lat)
>
># Feed the x and y to the scatter plot
>iceland.scatter(eqX, eqY, color='green', s=eq_size, alpha=0.6)
>
># Add title to the figure
>plt.title('Earthquakes in Iceland')
>plt.show()
>```
> It does not really matter if the plot comes before or after rendering of the map, as long as it's all under the same `plt.figure()`. At most it might effect the order of which the items render, or the order of legends describing the plot
>
>![Basemap](../images/matplotlib-basemap-scatter.png)