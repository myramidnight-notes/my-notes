# Python: Files

```py
#array to collect the lines from file
collection = [] 

# Open the file
file_w = open("file name/path", "r")

# loop through the lines in the file
for line in file_w:
    self.collection.append(line.strip())

# close the file when done
file_w.close()
```